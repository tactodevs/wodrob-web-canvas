'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp',['ngRoute']).
config(['$routeProvider', function($routeProvider) {
      var basePath = '../scripts/views/views';
      $routeProvider
          .when('/create', {templateUrl: '../scripts/views/create.html',    controller: 'createCtrl'})
          .when('/mash', {templateUrl: '../scripts/views/mashupp.html',   controller: 'mashCtrl'})
          .when('/shop', {templateUrl: '../scripts/views/shop.html',      controller: 'shopCtrl'})
          .when('/', {templateUrl: '../scripts/views/home.html',      controller: 'homeCtrl'});
  $routeProvider.otherwise({redirectTo: '/error.html'});
}]);
