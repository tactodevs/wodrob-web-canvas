/**
 * Created by ved on 27/11/14.
 */

var app = angular.module('fileApp',[]);

app.controller('fileCtrl',['$scope', function ($scope) {
    $scope.detail = false;

    var dropArea = document.getElementById("dropbox");

    // initializing  event handlers
    dropArea.addEventListener("dragenter", function (evt) {
        evt.stopPropagation();
        evt.preventDefault();
    }, false);
    dropArea.addEventListener("dragleave", function(){
        evt.stopPropagation();
        evt.preventDefault();
    }, false);
    dropArea.addEventListener("dragover", function(evt) {
        evt.stopPropagation();
        evt.preventDefault();
    }, false);

    dropArea.addEventListener("drop", function(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        var files = evt.dataTransfer.files;
        if (files.length > 0) {
            $scope.$apply(function(){
                $scope.files = [];
                for (var i = 0; i < files.length; i++) {
                    $scope.files.push(files[i]);
                }
            });
        }
    }, false);

    //Drag and drop functionality
    $scope.setFiles = function(element) {
        $scope.$apply(function($scope) {
            $scope.files = [];
            for (var i = 0; i < element.files.length; i++) {
                $scope.files.push(element.files[i])
            }
            $scope.progressVisible = false
        });
    };

    //Function to upload file to the server
    $scope.uploadFile = function() {
        var formData = new FormData();
        for (var i in $scope.files) {
            formData.append("Files", $scope.files[i]);
        }
        var request = new XMLHttpRequest();
        request.upload.addEventListener("progress", uploadProgress, false);
        request.addEventListener("load", transferComplete, false);
        request.addEventListener("error", transferFailed, false);
        request.addEventListener("abort", transferCanceled, false);
        request.open("POST", "http://localhost:63342/fileupload",true);
        $scope.progressVisible = true;
        request.send(formData);
    };

    //Function to calculate size of file being uploading
    function uploadProgress(evt) {
        $scope.$apply(function(){
            if (evt.lengthComputable) {
                $scope.progress = Math.round(evt.loaded * 100 / evt.total)
            } else {
                $scope.progress = 'unable to compute';
            }
        });
    }

    //Function to display response from server
    function transferComplete(evt) {
        alert(evt.target.responseText)
    };

    //Function to display response
    function transferFailed(evt) {
        alert(" Error in attempting to upload the file.");
    };

    //Function to display response
    function transferCanceled(evt) {
        $scope.$apply(function(){
            $scope.progressVisible = false
        });
        alert("The upload has been canceled by the user.")
    }

}]);
