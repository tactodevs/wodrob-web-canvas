
var app =  angular.module('myApp', ['ui.bootstrap']);

//controller for modal
app.controller('modalCtrl',[ '$scope', '$modalInstance', 'modalProvider', 'popup', function ($scope,$modalInstance,modalProvider,popup) {
    'use strict';
    $scope.cancel = function () {
        $modalInstance.dismiss();
    };
    $scope.pattern = '';
    $scope.model = {};
    $scope.emailPattern = /^([a-zA-Z0-9])+([a-zA-Z0-9._%+-])+@([a-zA-Z0-9_.-])+\.(([a-zA-Z]){2,6})$/;

    $scope.getImgId =popup.src;
    console.log(popup);
    console.log('$scope.getImgId',$scope.getImgId);
    console.log('$scope.getImgId',popup.height);

    //var cropper = function () {
    //    var img = popup[0].href.baseVal;
    //    paper = Raphael('cropper',635,500);
    //var  image = paper.image(img,100,100,200,200);
    //    canvas= document.getElementById('cropper').style.backgroundColor='gray';
    //};
    //
    //setTimeout(function () {
    //    cropper();
    //},100);
    //CROP

    $scope.validate = function () {
        $scope.msg =  $scope.model.password === $scope.model.passwordConfirm  ? false :true;
    };
    $scope.model = {};
    $scope.details = function () {
        console.log($scope.model);
    };

    if (popup) {
        switch (popup) {
        case 'signin':
            $scope.signIn = true;
            $scope.signUp = false;
            break;
        case 'signup':
            $scope.signIn = false;
            $scope.signUp = true;
            break;
        }
    }

    $scope.forgotModal = function () {
        modalProvider.forgotPassword();
        $modalInstance.dismiss();
    };

    $scope.swipeModal = function (e) {
        switch (e) {
        case 'up':
            $scope.signIn = true;
            $scope.signUp = false;
            break;
        case 'in':
            $scope.signIn = false;
            $scope.signUp = true;
            break;
        }
    };
}]);

//services for modal
app.service('modalProvider',['$modal', function ($modal){

    //signIN PopUp
    this.signInPopup= function(val) {
        $modal.open({
            backdrop: true,
            keyboard: false,
            backdropClick: false,
            templateUrl: "../../scripts/views/sign-in.html",
            dialogClass: "modal-content",
            controller: 'modalCtrl',
            resolve:{
                popup : function () {
                    return val;
                }
            }
        });
    };

    this.cropPopup= function(e) {
        $modal.open({
            backdrop: false,
            keyboard: false,
            backdropClick: false,
            templateUrl: "../../scripts/views/image-crop.html",
            dialogClass: "modal-content",
            controller: 'modalCtrl',
            resolve:{
                popup : function () {
                    return e;
                }
            }
        });
    };

    //Forgot password Popup
    this.forgotPassword=function () {
        $modal.open({
            backdrop: true,
            keyboard: false,
            backdropClick: false,
            templateUrl: "../../scripts/views/forgot-password.html",
            dialogClass: "modal-content",
            controller: 'modalCtrl',
            resolve:{
                popup : function () {
                    return 0;
                }
            }
        });
    };

    //Feedback popUp
    this.feedBack = function () {
        $modal.open({
            backdrop: true,
            keyboard: false,
            backdropClick: false,
            templateUrl: "../../scripts/views/feedback.html",
            dialogClass: "modal-content",
            controller: 'modalCtrl',
            resolve:{
                popup : function () {
                    return 0;
                }
            }
        });
    }

}]);


//home page controller
app.controller('homepageCtrl', ['$scope', 'modalProvider', function ($scope,modalProvider) {

    //opens sign-in popup window
    //

    $scope.printText = function() {

    };
    $scope.signIn=function(e){
        modalProvider.signInPopup(e)
    };

    $scope.feedback = function() {
        modalProvider.feedBack();
    };

}]);


app.controller('createCtrl', ['$scope', 'modalProvider', '$compile', function ($scope, modalProvider,$compile) {
    'use strict';

    var ft, mainSvg;

    var undoArray = [], redoArray = [];
    $scope.elements = [];
    $scope.currentImage = null;
    $scope.opacity = 1;

    var paper = Raphael("editor", 790, 500);

    //Image array
    $scope.imageArray = [
        {'src': 'WodRob.png'},
        {'src': 'top.jpg'},
        {'src': 'coat.jpg'},
        {'src': 'top.jpg'}
    ];

    // click event handler on canvas
    $('#editor').click(function () {
        if ($scope.currentImage !== null) {
            $scope.currentImage.freeTransform.hideHandles();
        }
        $scope.$apply(function () {
            $scope.currentImage = null;
        });
    });

    //Getting the cursor position
    $scope.coOrdinate = function (x, y) {
        $scope.xc = x;
        $scope.yc = y;
    };

    //Captures Image transformation
    var getUndoAttrs= [];
    var getRedoAtrrs= [];
    $scope.attrs ={};
    $scope.captureEvent = function () {
        //console.log(' $scope.attrs ={};', $scope.attrs)
        //if($scope.currentImage.freeTransform != null){
        //    $scope.attrs = {x:$scope.currentImage.freeTransform.attrs.translate.x,y:$scope.currentImage.freeTransform.attrs.translate.y,
        //        r:$scope.currentImage.freeTransform.attrs.rotate};
        //    getUndoAttrs.push($scope.attrs);
        //    console.log('data.attrs',getUndoAttrs);
        //}
        var paperJSON = paper.toJSON(function (el, data) {

            if (el.node.localName === "image") {
                return data;
            }
        });
        undoArray.push(paperJSON);
    };

    //getting current image id.
    $scope.getId = function (ev) {
        $scope.imageId = ev;
    };

    //function to drag and drop elements
    var moves = function() {
        mainSvg = document.getElementsByTagName("svg")[0];
        mainSvg.addEventListener("dragenter", function (e)
        {e.preventDefault(); }, false);

        mainSvg.addEventListener("dragover", function (e)
        {e.preventDefault();},false);

        mainSvg.addEventListener("drop",dropped,false);
    };

    // image opacity
    $scope.clickHandler = function(el){
        $scope.$apply(function () {
            $scope.opacity = el.attr().opacity;
        });
        $scope.boxReframe(el);
    };


    //New canvas

    var dimension = [];
    $scope.newCanvas = function () {
        //$scope.currentImage.freeTransform.hideHandles();
        if($scope.currentImage!==null){
            $scope.currentImage.freeTransform.hideHandles();
            //$scope.currentImage=null;
        };

        //console.log('$scope.currentImage',$scope.currentImage);

        //var paperJSON = JSON.parse(paper.toJSON(function (el, data) {
        //
        //    if (el.node.localName === "image") {
        //        return data;
        //    }
        //}));
        var bot = paper.bottom;
        console.log(bot);
        dimension.push({dimension:bot._,imgSrc:bot.attrs.src});

        if(bot.next!==null){
            console.log('if');
            //console.log('aa',bot.node);
            while(bot.next!== null) {
                bot = bot.next;


                //console.log('bot',bot.next.type);

                dimension.push({dimension:bot._,imgSrc:bot.attrs.src});
                //console.log('a',bot.next);
                //if(bot.next.type==='path'){
                //    console.log('path');
                //    break;
                //}
            }

        }
        console.log('ele',(dimension));
        //console.log('ele',(bot.next));
        //console.log('ele1',paperJSON[0]);

        //var svg = paper.toSVG();
        //console.log('svg',svg);
        //document.getElementById('bar').innerHTML = svg;
        //canvg(document.getElementById('myCanvas'), svg);
        //setTimeout(function() {
        //    var dataURL = document.getElementById('myCanvas').toDataURL("image/png");
        //    //document.getElementById('myImg').src = dataURL;
        //    console.log('dataURL',dataURL);
        //}, 100);

        //if(confirm("Unsaved changes in the editor,Do you want to continue"))
        //{
        //    paper.clear();
        //    undoArray =[];
        //    redoArray =[];
        //    $scope.elements =[];
        //    $scope.currentImage=null;
        //}
        //else
        //{
        //    //do nothing
        //}
    };

    //image dropped event
    function dropped (e) {
        var img = $scope.imageId;
        var xpos= e.clientX - $scope.xc;
        var ypos = e.clientY - $scope.yc;
        var image = paper.image(img.src, xpos, ypos,img.width,img.height).click(function () {
            $scope.currentImage = this;
            $scope.clickHandler(this);
        });

        image.attr({
            cursor:'move',
            opacity: 1
        });

        $scope.currentImage=image;
        $scope.opacity = 1;


        $scope.$apply(function () {
            $scope.currentImage.attr().opacity= 1;
        });

        $scope.boxReframe(image);
        $scope.elements.push(image);
        console.log('image',image);
        $scope.captureEvent();
    }


    $scope.boxReframe = function(el) {

        // callback function
        var cb = function(obj,e) {
            var event = e.toString().split(",");

            event.forEach(function(e){
                if (e.toString() === 'rotate end') {
                    //ft.updateHandles();
                    ft.apply();
                    console.log('rotate $scope image',$scope.currentImage);
                    $scope.captureEvent();
                }
                else if (e.toString() === 'scale end') {

                    //$scope.attrs = {x:el.freeTransform.attrs.translate.x,y:el.freeTransform.attrs.translate.y,
                    //    r:el.freeTransform.attrs.rotate};
                    //console.log(' $scope.attrs', $scope.attrs);
                    //ft.updateHandles();
                    //ft.apply();
                    console.log('scale',$scope.currentImage);
                    //console.log('matrix',el.matrix.split());
                    $scope.captureEvent();

                }
                else if (e.toString() === 'drag end') {
                    //ft.apply();
                    console.log('drag end $scope image',$scope.currentImage);
                    $scope.captureEvent();
                }
            });
        };

        ft = paper.freeTransform(el,{draw:['bbox'],
             rotate: true,keepRatio:[ '-axisX', 'axisY', 'bboxCorners', 'bboxSides'],range: { rotate: [ -180, 180 ] },
             },cb);

        paper.forEach(function(elements) {
                    if(elements.freeTransform !=null && elements.freeTransform.handles.bbox != null)
                        elements.freeTransform.hideHandles();
                    event.stopPropagation();
                });
        el.freeTransform.showHandles();
    };


    //Function for moving back.
    $scope.back = function () {
        if($scope.currentImage !==null) {
            if($scope.currentImage.prev!=undefined) {
                var bot =  $scope.currentImage.prev;
                $scope.currentImage.insertBefore(bot);
                //ft.apply();
                $scope.captureEvent();
            }
        }
    };

    //Function for moving front
    $scope.front = function () {
        if($scope.currentImage !==null) {
            if($scope.currentImage.next!=undefined) {
                var top =  $scope.currentImage.next;
                if($scope.currentImage.next.node.localName == "image")
                $scope.currentImage.insertAfter(top);
                //ft.apply();
                $scope.captureEvent();
            }
        }
    };



    // Function to remove all elements from paper
    //$scope.clear = function () {
    //    paper.clear();
    //    $scope.currentImage = null;
    //    $scope.captureEvent();
    //};

    //Function to rotate element 180 d.
    $scope.flipImage = function () {
        if($scope.currentImage !==null){
            var ft = paper.freeTransform($scope.currentImage);
            ft.attrs.rotate = ft.attrs.rotate + 180;
            if(ft.attrs.rotate === 360){
                ft.attrs.rotate=0;
            }
            ft.apply();
            $scope.captureEvent();
        }
    };

    //Function to rotate element in 90d. cycle.
     $scope.flopImage = function () {

        if ($scope.currentImage !== null) {
            var ft = paper.freeTransform($scope.currentImage);
            $scope.currentImage.transform("S-1,1");
            //var x =  ft.attrs.scale.x;
            ft.attrs.scale.x=-ft.attrs.scale.x;
            ft.apply();
            console.log(ft.attrs);
            //console.log($scope.currentImage);
            //$scope.captureEvent();
        }
    };

    //Function for removing individual elements.
    $scope.deleteImage= function () {
        if($scope.currentImage !==null) {
            var i = _.indexOf($scope.elements,$scope.currentImage);
            $scope.elements.splice(i,1);
            $scope.currentImage.freeTransform.hideHandles();
            $scope.currentImage.remove();
            $scope.currentImage =null;
            $scope.captureEvent();
        }
    };

    //Undo operation
    $scope.undoOperation = function () {

        //if($scope.currentImage != null) {
        //    $scope.currentImage.freeTransform.hideHandles();
        //    $scope.currentImage = null;
        //}

        if($scope.elements.length>0) {
            console.log('undoArray',undoArray);

            var undoElement = undoArray.pop();
            redoArray.push(undoElement);

            var applyUndo = _.last(undoArray);

            paper.clear();
            $scope.elements = [];
            paper.fromJSON(applyUndo,function(el, data) {

                    // callback function
                    var cb = function(obj,e) {

                        //console.log(e);
                        var event = e.toString().split(",");
                        console.log('event',e.toString());

                        event.forEach(function(e){
                            if (e.toString() === 'rotate end') {
                                console.log('rotate image',$scope.currentImage);
                                $scope.captureEvent();
                            }
                            else if (e.toString() === 'scale end') {
                                console.log('scale image undo',$scope.currentImage);
                                $scope.captureEvent();
                            }
                            else if (e.toString() === 'drag end') {
                                console.log('drag end $scope image',$scope.currentImage);
                                $scope.captureEvent();
                            }
                        });
                    };
            //
            //var getData = getUndoAttrs.pop();
            //    getRedoAtrrs.push(getData);
            //    console.log('getRedoAtrrs',getRedoAtrrs);
            //    console.log('getUndoAttrs',getUndoAttrs);
            //    var setData = _.last(getUndoAttrs);
            //    console.log('setData.x',setData);

         ft =  paper.freeTransform(el,{draw:['bbox'],
                rotate: true,keepRatio:[ 'axisX', 'axisY', 'bboxCorners', 'bboxSides'],
                scale:[ 'axisX', 'axisY', 'bboxCorners', 'bboxSides' ]},cb);
                console.log('el',el);

                //el.freeTransform.attrs.translate.x =setData.x;
                //el.freeTransform.attrs.translate.y=setData.y;
                //el.freeTransform.attrs.rotate=setData.r;
                //console.log('Matrix',el.matrix.split());

            el.click(function () {
                paper.forEach(function(el) {
                    if(el.freeTransform.handles.bbox != null) {
                        el.freeTransform.hideHandles();
                    }
                });

                $scope.$apply(function () {
                    $scope.currentImage=this;
                });
                $scope.currentImage=this;
                var ft = paper.freeTransform($scope.currentImage);
                $scope.currentImage.freeTransform.showHandles();
               //$scope.currentImage.matrix.split().dx= $scope.currentImage.freeTransform.attrs.translate.x;
               // $scope.currentImage.matrix.split().dy=   $scope.currentImage.freeTransform.attrs.translate.y;
               // $scope.currentImage.matrix.split().rotate=$scope.currentImage.freeTransform.attrs.rotate ;

                //console.log('$scope.currentImage',$scope.currentImage);
                //console.log('Matrix',$scope.currentImage.matrix.split());

               ft.apply();
                event.stopPropagation();
            });

            if(el.node.localName == "image"){
                $scope.elements.push(el.node);
                return el;
            }
            });

            paper.forEach(function(el) {
                if(el.freeTransform.handles.bbox != null) {
                    el.freeTransform.hideHandles();
                }
            });
        }
    };
    $scope.saveCanvas = function () {

        if($scope.currentImage!==null){
            $scope.currentImage.freeTransform.hideHandles();
        }
        var bot = paper.bottom;
        dimension.push({dimension:bot._,imgSrc:bot.attrs.src});

        if(bot.next!==null){
            while(bot.next!== null) {
                bot = bot.next;
                dimension.push({dimension:bot._,imgSrc:bot.attrs.src});
            }
        }

        console.log('ele',(dimension));
        var svg = paper.toSVG();
        //console.log('svg',svg);
        //document.getElementById('bar').innerHTML = svg;
        canvg(document.getElementById('myCanvas'), svg);
        setTimeout(function() {
            var dataURL = document.getElementById('myCanvas').toDataURL("image/png");
            //document.getElementById('myImg').src = dataURL;
            console.log('dataURL',dataURL);
        }, 100);


    };


    //Redo operation
    $scope.redoOperation = function () {
        if(redoArray.length>0) {
            var redoElement = redoArray.pop();
            paper.clear();
            paper.fromJSON(redoElement,function(el, data) {

                // callback function
                var cb = function(obj,e) {
                    var event = e.toString().split(",");

                    event.forEach(function(e){
                        if (e.toString() === 'rotate end') {
                            //console.log('rotate');
                            console.log('image',$scope.currentImage);
                            $scope.captureEvent();
                        }
                        else if (e.toString() === 'scale end') {
                            //console.log('scale');
                            console.log('image',$scope.currentImage);
                            $scope.captureEvent();
                        }
                        else if (e.toString() === 'drag end') {
                            //console.log('drag');
                            console.log('image redo',$scope.currentImage);
                            $scope.captureEvent();
                        }
                    });
                };


                paper.freeTransform(el,{draw:['bbox'],
                    rotate: true,keepRatio:[ 'axisX', 'axisY', 'bboxCorners', 'bboxSides'],
                    scale:[ 'axisX', 'axisY', 'bboxCorners', 'bboxSides' ]},cb);

                //var getData = getRedoAtrrs.pop();
                //console.log('getRedoAtrrs',getRedoAtrrs);

                //getUndoAttrs.push(getData);
                //console.log('getUndoAttrs',getUndoAttrs);
                //var setData = _.last(getRedoAtrrs);
                //console.log('setData',setData);
                //console.log('getData.x',getData.x);

                //el.freeTransform.attrs.translate.x =getData.x;
                //el.freeTransform.attrs.translate.y=getData.y;
                //el.freeTransform.attrs.rotate=getData.r;

                el.click(function () {
                    paper.forEach(function(el) {
                        if(el.freeTransform.handles.bbox != null) {
                            el.freeTransform.hideHandles();
                        }
                    });

                    $scope.$apply(function () {
                        $scope.currentImage=this;
                    });

                    $scope.currentImage=this;
                    var ft = paper.freeTransform($scope.currentImage);
                    $scope.currentImage.freeTransform.showHandles();
                    //$scope.currentImage.matrix.split().dx=$scope.currentImage.freeTransform.attrs.translate.x;
                    //$scope.currentImage.matrix.split().dy=$scope.currentImage.freeTransform.attrs.translate.y;
                    //$scope.currentImage.freeTransform.attrs.rotate =$scope.currentImage.matrix.split().rotate;

                    //console.log('$scope.currentImage',$scope.currentImage);
                    //console.log('Matrix',$scope.currentImage.matrix.split());

                    ft.apply();
                    event.stopPropagation();
                });

                if(el.node.localName == "image"){
                    $scope.elements.push(el.node);
                    return el;
                }
            });

            paper.forEach(function(el) {
                if(el.freeTransform.handles.bbox != null) {
                    el.freeTransform.hideHandles();
                }
            });



            $scope.captureEvent();
        }

    };

    //function for cloning element
    $scope.cloneImage = function () {

        if($scope.currentImage !==null) {
            var cloned = $scope.currentImage.clone().click(function () {
                $scope.$apply(function () {
                    $scope.currentImage= this;
                });

                $scope.currentImage= this;
                $scope.boxReframe(cloned);
                console.log('$scope.currentImage',$scope.currentImage)
            });
            cloned.attr("x",$scope.currentImage.attr("x")+15);
            cloned.attr("y",$scope.currentImage.attr("y")+15);

            $scope.currentImage.freeTransform.hideHandles();
            $scope.currentImage = cloned;
            $scope.elements.push(cloned);
            $scope.boxReframe(cloned);
            ft.apply();
            $scope.captureEvent();
        }
    };

    //Function to open the crop Model
    $scope.cropPop = function () {
        if($scope.currentImage !==null) {
            modalProvider.cropPopup($scope.currentImage.attrs);
        }
    };

    //Function To change the opacity
    $scope.setOpacity= function () {
        if($scope.currentImage != null){
            $scope.currentImage.attr({
                opacity:$scope.opacity
            })
        }

    };


    //ZoomIn function
    $scope.zoomIn = function () {
        if ($scope.currentImage!= null) {
            var ft = paper.freeTransform($scope.currentImage);
            if(ft.attrs.scale.y<4) {
                $scope.currentImage.toFront();
                ft.attrs.scale.y = ft.attrs.scale.y  *(1.1);
                ft.attrs.scale.x = ft.attrs.scale.x  *(1.1);
                ft.apply();
                ft.updateHandles();
                $scope.captureEvent();
            }

        }
    };

    //zoomOut function
    $scope.zoomOut = function () {
        if ($scope.currentImage!= null) {
            var ft = paper.freeTransform($scope.currentImage);
            if(ft.attrs.scale.y>0.5) {
                $scope.currentImage.toFront();
                ft.attrs.scale.y = ft.attrs.scale.y /(1.1);
                ft.attrs.scale.x = ft.attrs.scale.x /(1.1);
                ft.apply();
                $scope.captureEvent();
            }

        }

    };
    moves();


    //Keyboard shortcuts

    //Delete
    Mousetrap.bind('ctrl+x', function(e) {
        if($scope.currentImage!=null) {
            $scope.deleteImage();
        }
    });

    //undo
    Mousetrap.bind('ctrl+z', function(e) {
        if($scope.currentImage!=null) {
            $scope.undoOperation();
        }

    });

    //Redo
    Mousetrap.bind('ctrl+shift+z', function(e) {
        if($scope.currentImage!=null) {
            $scope.redoOperation();
        }
    });

    //clone
    Mousetrap.bind('ctrl+c', function(e) {
        if($scope.currentImage!=null) {
            $scope.cloneImage();
        }
    });

     Mousetrap.bind('right', function(e) {
         //console.log('right',$scope.currentImage);

            if($scope.currentImage!=null) {
                console.log('right',$scope.currentImage);
                if($scope.currentImage.next!=undefined) {
                    var top =  $scope.currentImage.next;
                    if($scope.currentImage.next.node.localName == "image") {
                        $scope.currentImage.freeTransform.hideHandles();
                        $scope.currentImage = top;
                        $scope.currentImage.freeTransform.showHandles();
                        ft.apply();
                    }

                }
            }
        });

    Mousetrap.bind('left', function(e) {
            if($scope.currentImage!=null) {
                if($scope.currentImage.prev!=undefined) {
                    var bot =  $scope.currentImage.prev;
                        $scope.currentImage.freeTransform.hideHandles();
                        $scope.currentImage = bot;
                        $scope.currentImage.freeTransform.showHandles();
                        ft.apply();
                }
            }
        });

    //ZoomIN
    Mousetrap.bind('p', function(e) {
        if($scope.currentImage.freeTransform !=null) {
            $scope.zoomIn();
            event.preventDefault();

        }
    });

    //ZoomOut
    Mousetrap.bind('m', function(e) {
        if($scope.currentImage.freeTransform !=null) {
            $scope.zoomOut();
        }
    });



    //window.addEventListener("load",moves,false);
}]);

app.directive('forImage', function ($compile) {
    return {
        restrict:'CA',
        link: function (scope,elem,attr) {
            elem.on('mousedown', function (e) {
                var src = this;
                var  xc= e.offsetX,
                    yc= e.offsetY;
                scope.getId(src);
                scope.coOrdinate(xc,yc);
                $compile(elem)(scope)
            });
        }
    }
});
app.directive('allImage', function () {
    return {
        restrict:'A',
        link: function (scope,elem,attr) {
            elem.on('mousedown', function (e) {
                console.log('done');
            });
        }
    }
});
app.directive('raphaelDir', function($compile) {
    return {
        link : function(scope,element, attrs) {
            var paper = Raphael("editor", 650,500);
            var image =
                scope.passValue(paper,scope.image);
            scope.image.node.setAttribute('all-image', '');
            $compile(template)(scope);
        }
    };
});



//Image CROP controller
app.controller('cropCtrl',function ($scope) {
    $scope.rectFlag =0;
    $scope.polyFlag =0;
    var isFirstRect =0;
    var isFirstLine =0;
    var paper,canvas,rect,ft, line;
    var cropPoly= [], lineArray =[],drawLine =[], rectArray = [],clickPosition = [];
    var prevRect = null,rectWidth = 8;

    paper = Raphael("cropper", 280,300);
    //paper.setViewBox(50, 50, 280, 250 );
    //console.log('canvas',paper.canvas);
    //    canvas= document.getElementById('cropper').style.backgroundColor='gray';
    paper.image('../views/img/top.jpg', 5, 5,270,290);


    var initialize = function () {
        rect,ft, line=null;
        cropPoly= []; lineArray =[];
        isFirstRect = 0;
        isFirstLine = 0;
        drawLine =[];
        rectArray = [];
        prevRect = null;
        rectWidth = 8;
        clickPosition=[];
    };


    $scope.polyCrop = function () {
        $scope.polyFlag =1;$scope.rectFlag =0;
        initialize();
        //paper.clear();
        //paper.image('../views/img/top.jpg',  5, 5,270,290);
        paper.canvas.addEventListener("click",enablePolyCrop,false);
    };

    var posX, posY,rectX,rectY;
    var enablePolyCrop = function(e) {
        posX = e.offsetX ;
        posY = e.offsetY;
        rectX = posX - (rectWidth / 2);
        rectY = posY - (rectWidth / 2);
        clickPosition.push({x:posX,y:posY});

        rectArray.push(paper.rect(rectX, rectY, rectWidth, rectWidth).attr({fill: "#fff", opacity: 0.5}));

        if (isFirstRect === 0) {
            var getFirstPoint = rectArray[0].mousedown(function () {

                var boundPath=   paper.path(line + cropPoly).attr({
                    "type": "path",
                    "stroke": "none",
                    "fill": "rgba(255,255,255,0.6)"
                });

                paper.canvas.removeEventListener("click",enablePolyCrop,false);
                event.stopPropagation();

                rectArray.forEach(function(rec) {
                    rec.remove();
                });

                drawLine.forEach(function(li) {
                    li.remove();
                });
                initialize();
            });

        }
        isFirstRect = 1;

        if (prevRect) {
            var p = "M" + prevRect.x + " " + prevRect.y + "L" + posX + " " + posY;
            lineArray.push("L" + posX + " " + posY);
            drawLine.push(paper.path(p));
            cropPoly = lineArray.join();
        }
        prevRect = {x: posX, y: posY};
        if (isFirstLine === 0) {
            line = "M" + prevRect.x + " " + prevRect.y;
        }
        isFirstLine = 1;
    } ;

    $scope.undoCrop= function () {

        lineArray.pop();
        cropPoly = lineArray.join();
        if(rectArray.length>0) {
            var undoRect = rectArray.pop();
            undoRect.remove();
            clickPosition.pop();
            var getPoint= _.last(clickPosition);
            if(getPoint!=undefined) {
                prevRect.x =getPoint.x;
                prevRect.y =getPoint.y;
            }

        }

        if(drawLine.length>0) {
            var undoLine = drawLine.pop();
            undoLine.remove();
        }
        if(rectArray.length==0){
            initialize();
        }

        if($scope.rectFlag ===1) {
            $scope.rectCrop();
        }
    };


    $scope.cropImage = function () {
        if($scope.rectFlag==1) {
            var cropPath =rect.getBBox(),
                x = rect.getBBox().x,
                y = rect.getBBox().y,
                x1= rect.getBBox().height,
                y1 = rect.getBBox().width;
            image.attr({'clip-rect': [x,y,x1,y1]});
            rect.freeTransform.hideHandles();
            rect.attr({"stroke": "none"});
        }
    };

    var image;
    $scope.rectCrop = function () {
        paper.canvas.removeEventListener("click",enablePolyCrop,false);
        $scope.rectFlag =1;
        paper.clear();
        initialize();
        image=  paper.image('../views/img/top.jpg', 50, 50,200,250);

        rect = paper.rect(50, 50, 100, 100).attr({
            fill:"rgba(255,255,255,0.6)",
            stroke:"none"
        });

        ft = paper.freeTransform(rect,{draw:['bbox'],axes: [ 'x', 'y'],rotate: false,drag:true,
           
            scale:[ 'axisX', 'axisY', 'bboxCorners', 'bboxSides']});
        ft.handles.center.disc.hide();
        ft.handles.x.disc.hide();
        ft.handles.y.disc.hide();
        ft.handles.y.line.hide();
        ft.handles.x.line.hide();
        ft.handles.bbox[0].element.attr({
            fill:"rgba(0,0,5,0.6)",
            stroke:"none",
            width:8,
            height:8
        }); ft.handles.bbox[1].element.attr({
            fill:"rgba(0,0,5,0.6)",
            stroke:"none",
            width:8,
            height:8
        }); ft.handles.bbox[2].element.attr({
            fill:"rgba(0,0,5,0.6)",
            stroke:"none",
            width:8,
            height:8
        }); ft.handles.bbox[3].element.attr({
            fill:"rgba(0,0,5,0.6)",
            stroke:"none",
            width:8,
            height:8
        }); ft.handles.bbox[4].element.attr({
            fill:"rgba(0,0,5,0.6)",
            stroke:"none",
            width:8,
            height:8
        }); ft.handles.bbox[5].element.attr({
            fill:"rgba(0,0,5,0.6)",
            stroke:"none",
            width:8,
            height:8
        }); ft.handles.bbox[6].element.attr({
            fill:"rgba(0,0,5,0.6)",
            stroke:"none",
            width:8,
            height:8
        }); ft.handles.bbox[7].element.attr({
            fill:"rgba(0,0,5,0.6)",
            stroke:"none",
            width:8,
            height:8
        })

    }
});
