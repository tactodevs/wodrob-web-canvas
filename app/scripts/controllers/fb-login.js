/**
 * Created by ved on 9/12/14.
 */

var app = angular.module('fbApp',[]);
    app.controller('fbLogin',['$scope', function ($scope) {

        window.fbAsyncInit = function() {
            FB.init({
                appId      : '363245143846418',
                xfbml      : true,
                cookie     : true,
                version    : 'v2.2'
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        $scope.fbLogin = function() {

            FB.login(function (response) {
                FB.getLoginStatus(function(response) {
                    statusChangeCallback(response);
                })
            },{scope: 'user_birthday,user_location,user_friends'});
        };

        function statusChangeCallback(response) {
            console.log('statusChangeCallback');
            console.log(response);
            if (response.status === 'connected') {
                testAPI();
            } else if (response.status === 'not_authorized') {
                // The person is logged into Facebook, but not your app.
                document.getElementById('status').innerHTML = 'Please log ' +
                'into this app.';
            } else {
                // The person is not logged into Facebook, so we're not sure if
                // they are logged into this app or not.
                document.getElementById('status').innerHTML = 'Please log ' +
                'into Facebook.';
            }
        }

        function testAPI() {
            console.log('Welcome!  Fetching your information.... ');
            //function getUserInfo() {
                FB.api('/me', function(response) {
                    console.log(response);
                    console.log('location',response.birthday);
                    console.log(response.id);
                    console.log(response.email);
                    var id = response.id;
                    //console.log(graph.facebook.com/response.id/?fields=picture&type=large);
                    var img = 'http://graph.facebook.com/'+id +'/picture?type=large';
                    console.log(img);
                    $scope.user = {};
                    $scope.user.name= response.name;
                    $scope.user.email= response.email;

                    $scope.$apply(function () {
                        $scope.user.name= response.name;
                        $scope.user.email= response.email;
                        $scope.user.img= img;
                        $scope.user.gender= response.gender;
                        $scope.user.locale= response.locale;
                        $scope.user.timezone= response.timezone;
                        $scope.user.verified= response.verified;
                        $scope.user.birthday= response.birthday;
                    });
                });
        }
    }]);
