var app = angular.module('app', []);
app.controller('Ctrl',['$scope', function ($scope) {
    $scope.rating = 5;
    $scope.data = {};
    var update = function () {
        $scope.data.value = 500;
    };
    $scope.data.value= 500;
    $scope.check = function (t) {
        $scope.$watch(t, function (oldVal,newVal) {
            if(newVal > 1000 ) {

            }

        })
    }


}]);

app.directive('starRating',function() {

    return {

        //This template is used to display the star UX in repeted form.

        template: '<ul class="starRating">' + '   <label ng-repeat="star in stars" ng-class="star" ng-click="toggleFunck($index)">' + '\u2605' + '</ul>',

        scope: {

            ratingValue: '=',

            max: '=',

            onStarRating: '&'

        },

        link: function(scope, elem, attrs) {

            //This method is used to update the rating run time.

            var updateRating = function() {

                //This is global level collection.

                scope.stars = [];


                //Loop called with the help of data-max directive input and push the stars count.

                for (var i = 0; i < scope.max; i++) {

                    scope.stars.push({

                        filled: i < scope.ratingValue

                    });

                }

            };

            elem.bind('click', function () {

            });


            //This is used to toggle the rating stars.

            scope.toggleFunck = function(index) {

                //This is used to count the default start rating and sum the number of imput index.

                scope.ratingValue = index + 1;



                scope.onStarRating({

                    rating: index + 1

                });

            };


            //This is used to watch activity on scope, if any changes on star rating is call autometically and update the stars.

            scope.$watch('ratingValue',

                function(oldV, newV) {

                    if (newV) {

                        updateRating();

                    }

                }

            );


        }

    };

}

);


app.controller('path',function($scope) {

    function redrawArc (m) {
        var coords = getEndCoords(m);
        arc.attr({'path' : "M350,30 A 270 270 0 0 1 "+coords.x+" "+coords.y+""});
        trackball.attr({cx : coords.x, cy : coords.y, r : 12});
        txt.attr({x : coords.x, y : coords.y, text : coords.m});
    }
    /* Does the same as the above, but animates the motion. */
    function redrawArcAnimated (m) {
        var t = 500;
        var coords = getEndCoords(m);
        var arc_ani = Raphael.animation({'path' : "M350,30 A 270 270 0 0 1 "+coords.x+" "+coords.y+""}, t);
        var trackball_ani = Raphael.animation({cx : coords.x, cy : coords.y, r : 12}, t);
        var txt_ani = Raphael.animation({x : coords.x, y : coords.y, text : coords.m}, t);

        arc.animate(arc_ani);
        trackball.animate(trackball_ani);
        txt.animate(txt_ani);
    }
    /* Returns the endpoint of the arc given the number of minutes. */
    function getEndCoords (m) {
        //Get the angle and swivel the thing so it starts at the top, like a clock.
        var ang = 6 * m - 90;
        //Endpoint is given using simple trig.
        var x = 350 + 270 * Math.cos(ang * Math.PI / 180);
        var y = 300 + 270 * Math.sin(ang * Math.PI / 180);
        return {x : x, y : y, m : m};
    }


    var paper = new Raphael('raphbox');

    var coords = getEndCoords(15);

    var clockface = paper.circle(200, 200, 80)
        .attr({
            'stroke' : '#777777',
            'fill' : '#cccccc',
            'stroke-width' : 2
            //'transform' : 't-200,0'
        });
    var clocktxt = paper.text(350, 300, 'The Clock')
        .attr({
            'stroke-opacity' : "0",
            'fill' : "#333333",
            'font-family' : 'Arial',
            'font-size' : '18',
            'transform' : 't-200,0'
        });

    var arc = paper.path("M350,30 A 270 270 0 0 1 "+coords.x+" "+coords.y+"")
        .attr({
            'stroke' : '#3333cc',
            'stroke-width' : 7,
            'transform' : 't-200,0'
        });
    var ball = paper.circle(350, 30, 6)
        .attr({
            'stroke' : '#3333cc',
            'fill' : '#3333cc',
            'stroke-width' : 7,
            'transform' : 't-200,0'
        });
    var trackball = paper.circle(coords.x, coords.y, 12)
        .attr({
            'stroke' : '#3333cc',
            'fill' : '#3333cc',
            'stroke-width' : 7,
            'transform' : 't-200,0'
        });
    var txt = paper.text(coords.x, coords.y, coords.m)
        .attr({
            'stroke-opacity' : "0",
            'fill' : "#ffffff",
            'font-family' : 'Arial',
            'font-size' : '18',
            'transform' : 't-200,0'
        });

    /* Both buttons grab a random number from 1-30 and redraw the arc. */
//Click the cyan button to simply change the attributes.
    var btn = paper.rect(2,2,22,22).attr({'fill':'#33cccc'}).click(function(){
        redrawArc(Math.ceil(Math.random() * 30));
    });
//Click the magenta button to animate the same motion.
    var btn = paper.rect(32,32,22,22).attr({'fill':'#cc33cc'}).click(function(){
        redrawArcAnimated(Math.ceil(Math.random() * 30));
    });
    // Data
    var data = {
        "shapes": {
            "VED": {
                "type": "rect",
                "x": "0.3",
                "y": "0.01",
                "w": "60",
                "h": "40",
                "cr": "10",
                "cl": "#a44",
                "tx": "5",
                "ty": "5",
                "tcl": "#222",
                "t": "VED"
            },
            "NARESH": {
                "type": "rect",
                "x": "0.45",
                "y": "0.15",
                "w": "100",
                "h": "40",
                "cr": "10",
                "cl": "#a44",
                "tx": "5",
                "ty": "5",
                "tcl": "#222",
                "t": "NARESH"
            },
            "SAMEER": {
                "type": "rect",
                "x": "0.25",
                "y": "0.3",
                "w": "100",
                "h": "40",
                "cr": "10",
                "cl": "#a44",
                "tx": "5",
                "ty": "5",
                "tcl": "#222",
                "t": "SAMEER"
            },
            "GAURAV" : {
                "type": "rect",
                "x": "0.125",
                "y": "0.3",
                "w": "100",
                "h": "40",
                "cr": "20",
                "cl": "#a44",
                "tx": "5",
                "ty": "5",
                "tcl": "#222",
                "t": "GAURAV"
            }
        },
        "connections": {
            "0": {
                "s": "VED",
                "e": "NARESH",
                "cl": "#222",
                "bg": "#222|4"
            },
            "1": {
                "s": "NARESH",
                "e": "SAMEER",
                "cl": "#222",
                "bg": "#222|4"
            }, "2": {
                "s": "GAURAV",
                "e": "NARESH",
                "cl": "#222",
                "bg": "#222|2"
            }
        }
    };

// --------------------------------------
//  Function to draw shape connections
//
//  From raphaeljs.com/graffle.html
// --------------------------------------
    Raphael.fn.connection = function(obj1, obj2, line, bg) {
        if (obj1.line && obj1.from && obj1.to) {
            line = obj1;
            obj1 = line.from;
            obj2 = line.to;
        }
        var bb1 = obj1.node.getBBox(),
            bb2 = obj2.node.getBBox(),
            p = [{
                x: bb1.x + bb1.width / 2,
                y: bb1.y - 1},
                {
                    x: bb1.x + bb1.width / 2,
                    y: bb1.y + bb1.height + 1},
                {
                    x: bb1.x - 1,
                    y: bb1.y + bb1.height / 2},
                {
                    x: bb1.x + bb1.width + 1,
                    y: bb1.y + bb1.height / 2},
                {
                    x: bb2.x + bb2.width / 2,
                    y: bb2.y - 1},
                {
                    x: bb2.x + bb2.width / 2,
                    y: bb2.y + bb2.height + 1},
                {
                    x: bb2.x - 1,
                    y: bb2.y + bb2.height / 2},
                {
                    x: bb2.x + bb2.width + 1,
                    y: bb2.y + bb2.height / 2}],
            d = {},
            dis = [];
        for (var i = 0; i < 4; i++) {
            for (var j = 4; j < 8; j++) {
                var dx = Math.abs(p[i].x - p[j].x),
                    dy = Math.abs(p[i].y - p[j].y);
                if ((i == j - 4) || (((i != 3 && j != 6) || p[i].x < p[j].x) && ((i != 2 && j != 7) || p[i].x > p[j].x) && ((i != 0 && j != 5) || p[i].y > p[j].y) && ((i != 1 && j != 4) || p[i].y < p[j].y))) {
                    dis.push(dx + dy);
                    d[dis[dis.length - 1]] = [i, j];
                }
            }
        }
        if (dis.length == 0) {
            var res = [0, 4];
        } else {
            res = d[Math.min.apply(Math, dis)];
        }
        var x1 = p[res[0]].x,
            y1 = p[res[0]].y,
            x4 = p[res[1]].x,
            y4 = p[res[1]].y;
        dx = Math.max(Math.abs(x1 - x4) / 2, 10);
        dy = Math.max(Math.abs(y1 - y4) / 2, 10);
        var x2 = [x1, x1, x1 - dx, x1 + dx][res[0]].toFixed(3),
            y2 = [y1 - dy, y1 + dy, y1, y1][res[0]].toFixed(3),
            x3 = [0, 0, 0, 0, x4, x4, x4 - dx, x4 + dx][res[1]].toFixed(3),
            y3 = [0, 0, 0, 0, y1 + dy, y1 - dy, y4, y4][res[1]].toFixed(3);
        var path = ["M", x1.toFixed(3), y1.toFixed(3), "C", x2, y2, x3, y3, x4.toFixed(3), y4.toFixed(3)].join(",");
        if (line && line.line) {
            line.bg && line.bg.attr({
                path: path
            });
            line.line.attr({
                path: path
            });
        } else {
            var color = typeof line == "string" ? line : "#000";
            return {
                bg: bg && bg.split && this.path(path).attr({
                    stroke: bg.split("|")[0],
                    fill: "none",
                    "stroke-width": bg.split("|")[1] || 3
                }),
                line: this.path(path).attr({
                    stroke: color,
                    fill: "none"
                }),
                from: obj1,
                to: obj2
            };
        }
    };

// Initialize variables
    var r, connections, shapes, texts, canvasWidth, canvasHeight;
// On ready, draw design on canvas
    $(document).ready(function() {
        // Get canvas height and width
        canvasHeight = 400;
        canvasWidth = $('#StratusCanvas').width();
        if (canvasWidth == undefined) {
            canvasWidth = 400;
        }

        // Dragging functions
        var dragger = function() {
            // Original coords for main element
            if (this.type == "rect" || this.type == "text") {
                this.ox = this.attr("x");
                this.oy = this.attr("y");
            } else {
                this.ox = this.attr("cx");
                this.oy = this.attr("cy");
            }
            if (this.type != "text") {
                this.animate({
                    "fill-opacity": .2
                }, 500);
            }

            // Original coords for pair element
            if (this.pair.type == "rect" || this.pair.type == "text") {
                this.pair.ox = this.pair.attr("x");
                this.pair.oy = this.pair.attr("y");
            } else {
                this.pair.ox = this.pair.attr("cx");
                this.pair.oy = this.pair.attr("cy");
            }
            if (this.pair.type != "text") {
                this.pair.animate({
                    "fill-opacity": .2
                }, 500);
            }
        };
        var move = function(dx, dy) {
            // Move main element
            var att;
            if (this.type == "rect" || this.type == "text") {
                att = {
                    x: this.ox + dx,
                    y: this.oy + dy
                };
            } else {
                att = {
                    cx: this.ox + dx,
                    cy: this.oy + dy
                };
            }
            this.attr(att);

            // Move paired element
            if (this.pair.type == "rect" || this.pair.type == "text") {
                att = {
                    x: this.pair.ox + dx,
                    y: this.pair.oy + dy
                };
            } else {
                att = {
                    cx: this.pair.ox + dx,
                    cy: this.pair.oy + dy
                };
            }
            this.pair.attr(att);

            // Move connections
            for (i = connections.length; i--;) {
                r.connection(connections[i]);
            }
            r.safari();
        };
        var up = function() {
            // Fade original element on mouse up
            if (this.type != "text") this.animate({
                "fill-opacity": 0
            }, 500);
            // Fade paired element on mouse up
            if (this.pair.type != "text") this.pair.animate({
                "fill-opacity": 0
            }, 500);
        };

        // Initialize RaphaelJS
        r = Raphael("StratusCanvas", canvasWidth, canvasHeight);
        // Initialize connections, shapes, and texts
        connections = [];
        shapes = {};
        texts = [];

        // Load Design Flowchart JSON
        // Get data
        var cxns = data.connections;
        var shps = data.shapes;
        // Process shapes
        $.each(shps, function(key, val) {
            if (val.type == 'rect') {
                // Calculate shape position
                var shapeX = val.x;
                var shapeY = val.y;
                if (Number(val.x) < 1) {
                    shapeX = val.x * canvasWidth;
                }
                if (Number(val.y) < 1) {
                    shapeY = val.y * canvasHeight;
                }
                if (shapeX == undefined || isNaN(shapeX)) {
                    shapeX = 0;
                }
                if (shapeY == undefined || isNaN(shapeY)) {
                    shapeY = 0;
                }
                // Create rectangle
                var shape = r.rect(shapeX, shapeY, val.w, val.h, val.cr);
                // Add attributes
                shape.attr({
                    fill: val.cl,
                    stroke: val.cl,
                    "fill-opacity": 0,
                    "stroke-width": 2,
                    cursor: "move"
                });
                // Create text (position in center of shape)
                var text = r.text(shapeX + val.w / 2, shapeY + val.h / 2, val.t);
                // Color text
                text.attr({
                    fill: val.tcl,
                    stroke: "none",
                    "font-size": 15,
                    cursor: "move"
                });
                // Pair shape and text
                shape.pair = text;
                text.pair = shape;
                // Add drag functionality
                shape.drag(move, dragger, up);
                text.drag(move, dragger, up);
                // Add shape to shapes
                shapes[key] = shape;
                // Add text to texts
                texts.push(text);
            }
        });
        // Process connections
        $.each(cxns, function(key, val) {
            var line = r.connection(
                shapes[val.s], shapes[val.e], val.cl, val.bg);
            connections.push(line);
        });
    });
})

